import time
from dataclasses import dataclass
from itertools import permutations
from typing import Tuple, List, Dict

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import random

import math

from mpl_toolkits.mplot3d import Axes3D


class Item:

    def __init__(self, dim, sku='item'):
        self.dim = dim
        self.sku = sku

    def __repr__(self) -> str:
        return f'Item({", ".join(str(a) for a in self.dim)}, {self.sku})'

    def volume(self):
        return self.dim[0] * self.dim[1] * self.dim[2]


class Box:

    def __init__(self, dim, original_box=None, origin=(0, 0, 0)):
        self.dim = dim
        self.origin = origin
        if original_box:
            self.original_box = original_box
        else:
            self.original_box = self

    def __repr__(self) -> str:
        return f'Box({", ".join(str(a) for a in self.dim)})'

    def volume(self):
        return self.dim[0] * self.dim[1] * self.dim[2]

    def oriented_item_fits(self, dimensions: Tuple[float, float, float]) -> bool:
        a, b, c = dimensions
        x, y, z = self.dim
        return a <= x and b <= y and c <= z

    def create_subboxes(self, dimensions: Tuple[float, float, float]) -> List[List['Box']]:
        results = []
        for indices in permutations([0, 1, 2]):
            cur_dims = list(dimensions)
            subboxes = []

            for i in indices:
                new_dimensions = [None, None, None]
                new_dimensions[i] = self.dim[i] - dimensions[i]
                new_dimensions[(i + 1) % 3] = cur_dims[(i + 1) % 3]
                new_dimensions[(i + 2) % 3] = cur_dims[(i + 2) % 3]

                assert all(x >= 0 for x in new_dimensions)

                new_origin = list(self.origin)
                new_origin[i] += dimensions[i]

                subboxes.append(Box(tuple(new_dimensions), self.original_box, tuple(new_origin)))
                cur_dims[i] = self.dim[i]

            assert tuple(cur_dims) == tuple(self.dim)

            assert sum(sb.volume() for sb in subboxes) == self.volume() - dimensions[0] * dimensions[1] * dimensions[2]
            results.append(subboxes)

        return results

    def fit(self, item: Item):
        best_solution = None
        best_solution_value = -float('inf')

        for a, b, c in permutations(item.dim):
            if self.oriented_item_fits((a, b, c)):
                placements = self.create_subboxes((a, b, c))
                for placement in placements:
                    placement_value = max(sb.volume() for sb in placement)
                    if placement_value > best_solution_value:
                        best_solution = placement, Packing.PackedItem(
                            item,
                            self.origin,
                            (a, b, c)
                        )
                        best_solution_value = placement_value

        return best_solution


@dataclass
class Packing:
    @dataclass
    class PackedItem:
        item: Item
        pos: Tuple[float, float, float]
        dim: Tuple[float, float, float]

    box_contents: Dict[Box, List[PackedItem]]
    unpacked_items: List[Item]

    def get_assignment(self) -> Dict[Box, List[Item]]:
        return {k: [pi.item for pi in v] for (k, v) in self.box_contents.items()}

    def plot(self, box: Box):
        packed_items = self.box_contents[box]

        y, x, z = np.indices((box.dim[1], box.dim[0], box.dim[2]))

        colorsmap = mpl.cm.Set3.colors
        color_index = 0

        voxels = (x < 0) & (y < 0) & (z < 0)

        colors = np.empty(voxels.shape + (4,))

        fig = plt.figure(figsize=(15, 12), dpi=120)

        for i, pi in enumerate(packed_items):
            body = (x < box.dim[0] - pi.pos[0]) & (y >= pi.pos[1]) & (z >= pi.pos[2]) & (
                    x >= box.dim[0] - (pi.pos[0] + pi.dim[0])) & (
                           y < pi.pos[1] + pi.dim[1]) & (z < pi.pos[2] + pi.dim[2])
            colors[body, :] = colorsmap[color_index] + (0.8,)
            color_index = (color_index + 1) % len(colorsmap)
            voxels |= body

            sq = math.ceil(math.sqrt(len(packed_items)))

            ax = fig.add_subplot(sq, sq, i + 1, projection='3d')
            ax.voxels(voxels, facecolors=colors)
            ax.dist = 13
            ax.set_title(f'{i + 1}. Adding {pi.item.sku} with size {pi.item.dim}', fontsize=18)

        plt.savefig('pilt.png')
        plt.show()

    def plot_all(self):
        for box in self.box_contents.keys():
            self.plot(box)


def algo(boxes, items) -> Packing:
    boxes.sort(key=lambda x: x.volume())
    items.sort(key=lambda x: x.volume(), reverse=True)

    packing = {}
    unpacked = []

    for item in items:
        for box in boxes:
            solution = box.fit(item)
            if solution:
                new_boxes, packed_item = solution
                boxes.remove(box)
                boxes.extend(new_boxes)
                boxes.sort(key=lambda x: x.volume())

                original_box = box.original_box
                if original_box not in packing:
                    packing[original_box] = []
                packing[original_box].append(packed_item)
                break
        else:
            unpacked.append(item)

    return Packing(packing, unpacked)


# algo([Box([4, 4, 4])], [Item([1, 2, 3])] * 10).plot_all()
# algo([Box([3, 3, 3])], 28 * [Box([1, 1, 1])]).plot_all()
# algo([Box([3, 3, 3])], [Item([1, 1, 1], "asi1"), Item([3, 7, 8], "asi2")])
# print(algo([Box([10, 8, 7])], [Item([8, 6, 5], "asi1")]))

random.seed(4247)
boxes = [Box([5, 5, 5])]
items = []
for i in range(16):
    items.append(Item([random.randint(1, 5), random.randint(1, 5), random.randint(1, 5)]))

algo(boxes, items).plot_all()

# example = algo([Box([8, 6, 5])], [Item([5, 8, 4])])
# print(example)
# example.plot_all()

# items = [
#     Item([100, 2, 2]),
#     Item([3, 4, 5]),
#     Item([1, 1, 1]),
#     Item([2, 3, 4]),
#     Item([2, 2, 2]),
# ]
#
# boxes = [
#     Box([3, 6, 5]),
#     Box([3, 3, 3]),
# ]
#
# big_boxes = [
#    Box([1000, 1000, 1000]),
#    Box([1000, 1000, 1000]),
# ]
# more_items = [
#    Item([20, 30, 10]),
#    Item([10, 10, 10]),
#    Item([60, 100, 70]),
#    Item([400, 205, 111]),
#    Item([250, 500, 300]),
#    Item([50, 400, 33]),
# ]
#
# start = time.time()
# algo(big_boxes, more_items).plot_all()
# end = time.time()
# print(end - start)
