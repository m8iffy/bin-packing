import time


class Box:

    def __init__(self, dim):
        self.dim = sorted(dim)

    def volume(self):
        return self.dim[0] * self.dim[1] * self.dim[2]

    def fit(self, item):
        paigutus = [0, 0, 0]
        for di in item.dim:
            for i in range(3):
                if di <= self.dim[i] and paigutus[i] == 0:
                    paigutus[i] = di
                    break
            else:
                return None
        print(paigutus)
        return paigutus


class Item:

    def __init__(self, dim):
        self.dim = sorted(dim, reverse=True)

    def volume(self):
        return self.dim[0] * self.dim[1] * self.dim[2]


def algo(boxes, items):
    boxes.sort(key=lambda x: x.volume())
    items.sort(key=lambda x: x.volume(), reverse=True)

    for item in items:
        for box in boxes:
            paigutus = box.fit(item)
            if paigutus:
                print("laa")
                break
        else:
            print("asi mõõtmetega:", item.dim, "ei mahu.")


items = [
    Item([100, 2, 2]),
    Item([3, 4, 5]),
    Item([1, 1, 1]),
    Item([2, 3, 4]),
    Item([2, 2, 2]),
]

boxes = [
    Box([3, 6, 5]),
    Box([3, 3, 3]),
]
print(boxes, items)

big_boxes = [
    Box([1000, 1000, 1000]),
    Box([1000, 1000, 1000]),
]
more_items = [
    Item([20, 30, 10]),
    Item([10, 10, 10]),
    Item([60, 100, 70]),
    Item([400, 205, 111]),
    Item([250, 500, 300]),
    Item([50, 400, 33]),
]

start = time.time()
print(algo(big_boxes, more_items))
end = time.time()
print(end - start)
