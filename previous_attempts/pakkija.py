#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[104]:


class Box:

    def __init__(self, dim):
        self.dim = dim
        self.capacity = 0
        self.weight = 0
        self.space = np.zeros(dim)
        self.filled_volume = 0
        self.price = 0

    def volume(self):
        return self.dim[0] * self.dim[1] * self.dim[2]

    def add_item(self, item, start_xyz):
        if can_add_item(self, item, start_xyz):
            self.weight += item.weight
            a = np.ones(item.dim)
            self.space[start_xyz[0]:start_xyz[0] + item.dim[0], start_xyz[1]:start_xyz[1] + item.dim[1],
            start_xyz[2]:start_xyz[2] + item.dim[2]] = a
            self.filled_volume += item.volume()
            return True
        return False


def check_zero(self, dim, xyz):
    # fits = True
    if (xyz[0] + dim[0] < self.dim[0]) and (xyz[1] + dim[1] < self.dim[1]) and (xyz[2] + dim[2] < self.dim[2]):
        for i in range(dim[0]):
            for j in range(dim[1]):
                for k in range(dim[2]):
                    if self.space[xyz[0] + i, xyz[1] + j, xyz[2] + k] > 0:
                        return False
    else:
        return False
    return True


def can_add_item(self, item, xyz):
    if self.weight + item.weight <= self.capacity:
        if check_zero(self, item.dim, xyz):
            return True
    return False


# In[105]:


class Item:

    def __init__(self, dim):
        self.dim = dim
        self.weight = 0

    def volume(self):
        return self.dim[0] * self.dim[1] * self.dim[2]

    def rotate(self):
        a = self.dim[0]
        self.dim[0] = self.dim[1]
        self.dim[1] = self.dim[2]
        self.dim[2] = a

    def flip(self):
        a = self.dim[0]
        self.dim[0] = self.dim[2]
        self.dim[2] = a


# In[106]:


def algo_item_box(boxes, item):
    for box in boxes:
        if item.volume() + box.filled_volume <= box.volume():
            for x in range(box.dim[0]):
                for y in range(box.dim[1]):
                    for z in range(box.dim[2]):
                        for flip in range(1):
                            for rotate in range(3):
                                if can_add_item(box, item, [x, y, z]):
                                    box.add_item(item, [x, y, z])
                                    return True
                                item.rotate()
                            item.flip()
    return False


def algo(boxes, items):
    items.sort(key=lambda x: x.volume(), reverse=True)
    for item in items:
        if not algo_item_box(boxes, item):
            return "ERROR!"


# In[113]:


a = Item([2, 2, 2])
b = Item([3, 4, 5])
c = Item([1, 1, 1])
d = Item([2, 3, 4])
e = Item([2, 2, 2])
A = Box([3, 6, 5])
B = Box([3, 3, 3])
# print(A.dim[0])
Items = [a, b, c, d]
print(algo([A, B], Items))
print(A.space)
print(B.space)


C = Box([1000, 1000, 1000])
D = Box([1000, 1000, 1000])
f = Item([20, 30, 10])
g = Item([10, 10, 10])
h = Item([60, 100, 70])
i = Item([400, 205, 111])
j = Item([250, 500, 300])
k = Item([50, 400, 33])

# print(algo([C, D], [f, g, h, i, j, k]))
# print(C.space)
# print(D.space)
#

import time

start = time.time()
print(algo([C, D], [f, g, h, i, j, k]))
end = time.time()
print(end - start)

